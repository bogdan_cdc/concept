var gulp = require('gulp');
var handlebars = require('C:/Users/user/AppData/Roaming/npm/node_modules/handlebars');
var config = require('./gulp.config')();
var gulpHandlebars = require('C:/Users/user/AppData/Roaming/npm/node_modules/gulp-handlebars-html')(handlebars);
var regexRename = require('C:/Users/user/AppData/Roaming/npm/node_modules/gulp-regex-rename');
var replace = require('C:/Users/user/AppData/Roaming/npm/node_modules/gulp-replace');
var plugins = require('C:/Users/user/AppData/Roaming/npm/node_modules/gulp-load-plugins')();
var sass = require('C:/Users/user/AppData/Roaming/npm/node_modules/gulp-sass');
var sourcemaps = require('C:/Users/user/AppData/Roaming/npm/node_modules/gulp-sourcemaps'),
    autoprefixer = require('C:/Users/user/AppData/Roaming/npm/node_modules/gulp-autoprefixer');
    browserSync = require('C:/Users/user/AppData/Roaming/npm/node_modules/browser-sync').create();
var scsslint = require('C:/Users/user/AppData/Roaming/npm/node_modules/gulp-scss-lint');



var paths = {
  css: ['./scss/*.scss'],
  js: ['./js/*.js']
};



gulp.task('compileHtml', function () {
  var templateData = {
    
  },
  options = {
    partialsDirectory: [config.templatePartialPath]
  };
  
  return gulp.src(config.templatePath + "*.page.hbs")
  .pipe(gulpHandlebars(templateData, options))
  .pipe(regexRename(/\.page\.hbs$/, ".html"))
  .pipe(replace(/\uFEFF/ig, "")) //cut out zero width nbsp characters the compiler adds in
  .pipe(gulp.dest(config.templateOutputPath))
  .pipe(browserSync.reload({ stream:true }))
});

gulp.task('scss-lint', function() {
  return gulp.src('./scss/*.scss')
    .pipe(scsslint());
});

var options = {};

options.autoprefixer = {
  browsers: [
  "Android 2.3",
  "Android >= 4",
  "Chrome >= 20",
  "Firefox >= 24",
  "Explorer >= 7",
  "iOS >= 6",
  "Opera >= 12",
  "Safari >= 6"
  ]
};

//  css

gulp.task('prepros', function(){
    return gulp.src(paths.css)
    .pipe(sourcemaps.init())
    .pipe(sass({
      outputStyle: 'expanded'
    }).on('error', sass.logError))
    .pipe(autoprefixer(options.autoprefixer))
    .pipe(sourcemaps.write('./css/'))
    .pipe(gulp.dest('./css/'))
    .pipe(browserSync.reload({ stream:true }))
});

gulp.task('reloadJs', function(){
  return gulp.src(paths.js)
              .pipe(browserSync.reload({ stream:true }))
});

gulp.task('watcher',function(){
    gulp.watch(paths.css, ['prepros']);
    gulp.watch(paths.js, ['reloadJs']);
    gulp.watch(config.templates, ['compileHtml']);
});

browserSync.init({
  open: false,
  injectChanges: true,
  server: "./",
  port: 8765
});

gulp.task('default', ['watcher', 'compileHtml']);