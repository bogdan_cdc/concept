module.exports = function () {
    var root = './';
    var assetRoot = root + './';
    var handlebarsRoot = assetRoot + 'templates/';

    var config = {
        templatePath: handlebarsRoot,
        templatePartialPath: handlebarsRoot + 'partials',
        templateOutputPath: handlebarsRoot + '../',
        templates: [
            handlebarsRoot + '**/*.hbs'
        ]
    };

    return config;
};