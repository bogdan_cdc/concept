$(document).ready(function() {

	// + mainpage slider

	if($('.mainSlider').length){
		var mainPageSwiper = new Swiper('.mainSlider', {
			speed: 700,
			spaceBetween: 10,
			parallax: true,
			pagination: {
				el: '.mainSlider__pagination',
				type: 'bullets',
				renderBullet: function (index, className) {
					return '<li class="mainSlider__pagination__item ' + className + '"></li>';
				},
				clickable: true
			},
			navigation: {
				nextEl: '.mainSlider__nav_next',
				prevEl: '.mainSlider__nav_prev',
			},
		});
	}

	// - mainpage slider


	$(".pre_banner .closer").on('click', function(){
		$(".pre_banner").hide();
	})

	var slider = document.getElementById('priceFilter');

	if (slider != undefined) {
		
		var rangeMaxVal = document.getElementById('maxVal');
		var rangeMinVal = document.getElementById('minVal');
		
		var rangeMax = parseInt(slider.getAttribute('data-max'));
		var rangeMin = parseInt(slider.getAttribute('data-min'));
		var rangeStart = [parseInt(slider.getAttribute('data-startMin')), parseInt(slider.getAttribute('data-startMax'))];
		var range_step = parseInt(slider.getAttribute('data-step'));
		
		noUiSlider.create(slider, {
			start: rangeStart,
			connect: true,
			range: {
				'min': rangeMin,
				'max': rangeMax
			},
			step: range_step
		});
	
		slider.noUiSlider.on('update', function(values, handle){
			var value = values[handle];
			if (handle) {
				rangeMaxVal.value = Math.round(value);
			} else {
				rangeMinVal.value = Math.round(value);
			}
		})
	}

    $('.pageSwiperThumbed').each(function() { //if need multiple galleries on a page
        var slider = '#' + $(this).attr('id'),
            slider_thumbs = '#' + $(this).data('thumbs');
        var galleryTop = new Swiper(slider, {
			effect: 'fade',
            spaceBetween: 10,
            slidesPerView: 1,
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },
            thumbs: {
                swiper: {
                    el: slider_thumbs,
                    spaceBetween: 10,
                    slidesPerView: 4,
                    freeMode: true,
                    watchSlidesVisibility: true,
                    watchSlidesProgress: true,
                    direction:'vertical',
                    breakpoints: {
                      768: {
                        direction:'horizontal',
					  },
					  992: {
                        direction:'vertical',
					  },
					  1366: {
                        direction:'horizontal',
					  }
                    }
                }
            }
        });  
    });

	if($('._baguette_gal').length){
		function initBag() {
			baguetteBox.run('._baguette_gal');
		}
		initBag();
	}

    $('.number-plus-minus').each(function(index, el) {
        $(this).find('input').wrap('<div class="jq-number"></div>');
        $(this).find('input').wrap('<div class="jq-number__field"></div>')
        $(this).find('.jq-number').prepend('<div class="jq-number__spin plus"></div>');
        $(this).find('.jq-number').append('<div class="jq-number__spin minus"></div>');
    });
    $('.jq-number__spin').on('click', function(event) {
        var input = $(this).parents('.number-plus-minus').find('input'),
            dataMin = parseInt($(input).attr('min')),
            dataMax = parseInt($(input).attr('max')),
            dataVal = parseInt($(input).val());
            if (dataMin >= dataVal && $(this).hasClass('minus')) {
                // do nothing
            } else if (dataMax <= dataVal && $(this).hasClass('plus')) {
                // do nothing
            } else if ($(this).hasClass('plus')) {
                dataVal = dataVal + 1;
            }  else if ($(this).hasClass('minus')) {
                dataVal = dataVal - 1;
            }
        $(input).val(dataVal);
    });


});

var pos_s = document.getElementsByClassName('pos_s')[0]

$('._menuTrig').on('click', function(e){
	e.stopPropagation()
	if ($('._menuTrig').next('.dropM').hasClass('show')) {
		$('.dropM').removeClass('show')
		$('._menuTrig').attr("aria-expanded","false");
	} else {
		$('.dropM').addClass('show')
		$('._menuTrig').attr("aria-expanded","true");
	}

	var nWidth = $(this).parents('.container').width() - $(this).parents('.container').find('.main_categories').width();
	$('.main_categories_wrap_right').each(function(){
		$(this).width(nWidth)
	});
})

$(document).on('click', function(e){
	if ($('.dropM').hasClass('show') && !$(e.target).parents('.dropM').length && !$(e.target).parents('._menuTrig').length) {
		$('.dropM').removeClass('show')
		$('._menuTrig').attr("aria-expanded","false");
	}
})

var disableScroll = false;
var scrollPos = 0;
function stopScroll() {
    disableScroll = true;
    scrollPos = $(window).scrollTop();
}
function enableScroll() {
    disableScroll = false;
}

jQuery(function($) {

	$('.popupNavTrigger').on('click', function(){
		$(this).toggleClass('is-active');

		ps1 = new PerfectScrollbar('._scrollable', {
		  wheelSpeed: 1,
		  wheelPropagation: false,
		  suppressScrollX: true
		});
	});

    $(window).bind('scroll', function(){
         if(disableScroll) $(window).scrollTop(scrollPos);
    });
    $(window).bind('touchmove', function(){
         $(window).trigger('scroll');
    });
	var ps1,
		ps2;
	$('._level_1 span').on('click', function(event) {
		event.stopPropagation();
		ps1 = new PerfectScrollbar('._scrollable', {
		  wheelSpeed: 1,
		  wheelPropagation: false,
		  suppressScrollX: true
		});
	});
	$('.hasChild').on('click', function(event) {
		// $('.mPopupNav__menu').addClass('fixed');
		if (ps1) {
			ps1.destroy();
		} 
		if (ps2) {
			ps2.destroy();
		}
		$(this).find('.childItem').first().addClass('active');
	});
	$('.childItem__goBackBtn span').on('click', function(event) {
		event.stopPropagation();
		$(this).parents('.childItem').first().removeClass('active');
	});
	$('.mPopupNavScroll').each(function(index, el) {
		const ps = new PerfectScrollbar(this, {
		  wheelSpeed: 1,
		  wheelPropagation: false,
		  suppressScrollX: true
		});
	});
	$('._close').on('click', function() {
	});

	$('.mPopupNav').on('hide.bs.modal', function () {
		if (ps1) {
			ps1.destroy();
		} 
		if (ps2) {
			ps2.destroy();
		}
		$('.popupNavTrigger').removeClass('is-active');
		$('._popupNav').find('.childItem').each(function() {
			$(this).removeClass('active');
		});
	})
	$('._pass_visibility_trig').on('click', function () {
		var pass = $(this).parents('.pass_field').find('#pass');
		if (pass.attr('type') == 'password') {
			pass.attr('type', 'text');
		} else {
			pass.attr('type', 'password');
		}
		$(this).find('.pas_icon_trig').toggle();
	})
	
	function scrollItAll (whatToScroll, whatToDo) {
		var ps1_scrollable_content;
		ps1_scrollable_content = new PerfectScrollbar(whatToScroll, {
			wheelSpeed: 1,
			wheelPropagation: false,
			suppressScrollX: true
		});
	}

	if ($('._scrollable_content').length) {
		$('._scrollable_content').each(function(index, el){
			var k = '._scrollable_content' + index;
			scrollItAll ( k )
		});
	}

	// + menu

	var $menu = $(".main_categories");
	$menu.menuAim({
		activate: activateSubmenu,
		deactivate: deactivateSubmenu
	});
	function activateSubmenu(row) {
		var $row = $(row);
		$row.addClass('active');
	}
	function deactivateSubmenu(row) {
		var $row = $(row);
		$row.removeClass('active');
	}

	// - menu



	$('.filters_trigger').on('click', function () {
		$('.sidebarNav').addClass()
	})



	$('.layoutTrigger__item').on('click', function(){
		if ($(this).hasClass('layoutTrigger__tileList')) {
			$(this).addClass('active');
			$(this).siblings().removeClass('active');
			$('.products_list')
				.removeClass('products_list__rowList')
				.addClass('products_list__tileList')
			$('.products_list').find('.col-sm-6').each(function(){
				$(this)
					.removeClass('col-lg-12')
					.addClass('col-xlg-3')
			})
		} else {
			$(this).addClass('active');
			$(this).siblings().removeClass('active');
			$('.products_list')
				.removeClass('products_list__tileList')
				.addClass('products_list__rowList')
			$('.products_list').find('.col-sm-6').each(function(){
				$(this)
					.removeClass('col-xlg-3')
					.addClass('col-lg-12')
			})
		}
	})

	$("#vendors").on('show.bs.collapse', function(e){
		var altText = $(this).parents('.sidebarNav__content').find('.more_button').data('alt_text');
		$(this).parents('.sidebarNav__content').find('.more_button').text(altText)
	})

	$("#vendors").on('hide.bs.collapse', function(e){
		var altText = $(this).parents('.sidebarNav__content').find('.more_button').data('original_text');
		$(this).parents('.sidebarNav__content').find('.more_button').text(altText)
	})




	//set defaults for modals
	var scrollWidth = window.innerWidth - document.documentElement.clientWidth;
	var modalIsOpen = false,
		modalIsAnimated = false;

	function simpleModalShow(target) {
		if ($(target).length && !modalIsAnimated) { //no act while animating is in proggress or no target specified
			modalIsOpen = true;
			$('body').addClass('modal-open');
			$('body').css('padding-right', scrollWidth);
			$(target).velocity("fadeIn", {
				duration: 300,
				begin: function () {
					modalIsAnimated = true;
					$(target).trigger('modal_show');
				},
				complete: function () {
					modalIsAnimated = false;
					$(target).trigger('modal_shown');
				}
			});
			$(target).removeClass('fade');
			$(target).addClass('show');
		} else {
			console.error('the target modal does not exist');
		}
	}

	function simpleModalHide() {

		if (!modalIsAnimated) {//no act while animating is in proggress
			$('.modal.show').velocity("fadeOut", {
				duration: 300,
				begin: function () {
					modalIsAnimated = true;
					$('.modal.show').trigger('modal_hide');
				},
				complete: function () {
                    $('body').removeClass('modal-open');
                    $('body').css('padding-right', '');
					$('.modal.show').removeClass('show');
					modalIsOpen = false;
					modalIsAnimated = false;
					$('.modal.show').trigger('modal_hidden');
				}
			});
		}
	}

	$(document).on('click', '.toggleModal', function (event) {
		event.preventDefault();
		var target = $(this).data('target');

		if (modalIsOpen) {
			simpleModalHide();
		} else {
			simpleModalShow(target);
			$('body').addClass('scrolledUp');
		}
	});

	$('.modal:not(._popupNav), ._closeBtn').on('click', function(event) {
		if ($(event.target).attr('class') == $(this).attr('class')) { //work around bubbling
			event.preventDefault();
			simpleModalHide();
		}
	});

	function placeFilters () {
		if ( $(window).width() < 992 ) {

			$("#toClone").appendTo('.modal-body');
			
		} else {

			$("#toClone").appendTo('.aside_col');

		}
	}

	placeFilters()

	$(window).resize(function () {
		placeFilters ()
		var nWidth = $(this).parents('.container').width() - $(this).parents('.container').find('.main_categories').width();
		$('.main_categories_wrap_right').each(function(){
			$(this).width(nWidth)
		});
	})
});