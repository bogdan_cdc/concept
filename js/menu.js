'use strict';

(function ($) {
	var mouseposy = 0,
		mouseposx = 0,
		firstTime = true;
	$('li .menu > li').mouseover(function (e) {
		if (Math.abs(mouseposx - e.screenX) < Math.abs(mouseposy - e.screenY) || mouseposx - e.screenX > 0 && !firstTime) {
			$(this).addClass('active');
			$(this).siblings().removeClass('active');
		} else if (firstTime) {
			$(this).addClass('active');
			$(this).siblings().removeClass('active');
		}
		firstTime = false;
		mouseposx = e.screenX;
		mouseposy = e.screenY;
	});
})(jQuery)